
const initialState = {
    encodedPass: '',
    password: '7091',
    currentValue: '',
    accessGranted: null
};

const reducer = (state = initialState , action) => {

    switch (action.type) {
        case 'NUMBER':
            if (state.currentValue.length > 3) {
                return state;
            }
            return {...state, currentValue: state.currentValue + action.value, encodedPass: state.encodedPass + '*'};
        case 'BACK':
            return {...state, currentValue: state.currentValue.slice(0, -1), encodedPass: state.encodedPass.slice(0, -1)};
        case 'CHECK':
            return state.currentValue === state.password ?
                {...state, currentValue: '', encodedPass: '', accessGranted: true} :
                {...state, currentValue: '', encodedPass: '', accessGranted: false};
        default:
            return state;

    }
};

export default reducer;