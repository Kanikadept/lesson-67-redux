import './App.css';
import Doorpass from "./containers/Doorpass/Doorpass";

const App = () => (
    <div className="App">
        <Doorpass/>
    </div>
);

export default App;
