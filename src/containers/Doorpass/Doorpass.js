import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import './Doorpass.css';

const Doorpass = () => {

    const dispatch = useDispatch();
    const state = useSelector(state => state);




    const handleClick = (event) => {
        dispatch({type: 'NUMBER', value: event.target.value});
    }

    const handleCheck = (event) => {
        dispatch({type: 'CHECK'});
    }

    const handleBack = (event) => {
        dispatch({type: 'BACK'});
    }

    let doorScreen = null;
    let screenCol = null;

    if (state.accessGranted === true) {
        doorScreen = <p>Access granted!</p>;
        screenCol = "screen access-granted";
    } else if (state.accessGranted === false) {
        doorScreen = <p>Access denied!</p>;
        screenCol = "screen access-denied";
    } else {
        screenCol = "screen";
        doorScreen = <p>{state.encodedPass}</p>;
    }

    return (
        <div className="door-pass">
            <div className={screenCol}>
                {doorScreen}
            </div>
            <div className="buttons">
                <div className="btn-wrapper">
                    <button onClick={handleClick} value="7">7</button>
                </div>
                <div className="btn-wrapper">
                    <button onClick={handleClick} value="8">8</button>
                </div>
                <div className="btn-wrapper">
                    <button onClick={handleClick} value="9">9</button>
                </div>
                <div className="btn-wrapper">
                    <button onClick={handleClick} value="4">4</button>
                </div>
                <div className="btn-wrapper">
                    <button onClick={handleClick} value="5">5</button>
                </div>
                <div className="btn-wrapper">
                    <button onClick={handleClick} value="6">6</button>
                </div>
                <div className="btn-wrapper">
                    <button onClick={handleClick} value="1">1</button>
                </div>
                <div className="btn-wrapper">
                    <button onClick={handleClick} value="2">2</button>
                </div>
                <div className="btn-wrapper">
                    <button onClick={handleClick} value="3">3</button>
                </div>
                <div className="btn-wrapper">
                    <button onClick={handleBack}>{"<"}</button>
                </div>
                <div className="btn-wrapper">
                    <button onClick={handleClick} value="0">0</button>
                </div>
                <div className="btn-wrapper">
                    <button onClick={handleCheck}>E</button>
                </div>
            </div>
        </div>
    );
};

export default Doorpass;